// -------------------------
// -- PROCESSING MAIN FNS --
// -------------------------

function setup() {
  background(config.color.background);
  frameRate(config.fps)
  createCanvas(config.canvas.w, config.canvas.h);
}

function draw() {
  update();

  noFill();
  noSmooth();
  background(config.color.background);

  stroke(config.color.cursor);
  ellipse(mouseX, mouseY, state.size);
  
  stroke(config.color.particle);
  state.nodes.map(n => point(n.x.x, n.x.y)); 
}

function update() {
  // TODO: Deal with collision
  if (mouseIsPressed) {
    var currT = new Date();
    state.nodes = state.nodes.concat(_.range((currT - state.prevT) * 0.005).map(i => {
      var dx = _.random(-1 * i, i);
      var dy = Math.pow(-1, _.random(0, 1)) * Math.sqrt(Math.pow(i, 2) - Math.pow(dx, 2));
      return createVector(
        createVector(
          mouseX + dx,
          mouseY + dy,
          0
        ), 0, false)
      }));

    state.size = (currT - state.prevT) * 0.01;
  }

  state.nodes.map(n => forceCollisionY(n, config.canvas.h - 25)).map(n => forceGravity(n, 1/60)).map(n => forceScreenBottom(n, config.canvas.h - 25, config.canvas.w));
}

function mousePressed() {
  state.prevT = new Date();
}

function mouseReleased() {
  state.size = 0;
}

// ----------
// -- DATA --
// ----------

const config = {
  canvas: {
    w: 320,
    h: 240
  },
  color: {
    background: 235,
    cursor: 180,
    particle: 0
  },
  fps: 60
}

// TODO: Maybe have nodes be of type int[config.canvas.w][config.canvas.h] to ensure one sand per pixel and easier collision detection
// Node structure: (x, y), vel, frozen
var state = {
  nodes: [],
  bottom: [],
  size: 0,
  prevT: null
}

// ----------------
// -- HELPER FNS --
// ----------------

function nest(seq, keys) {
    if (!keys.length)
        return seq;
    var first = keys[0];
    var rest = keys.slice(1);
    return _.mapValues(_.groupBy(seq, first), function (value) { 
        return nest(value, rest)
    });
};

function createArray(length) {
  var arr = new Array(length || 0),
      i = length;

  if (arguments.length > 1) {
    var args = Array.prototype.slice.call(arguments, 1);
    while(i--) arr[length-1 - i] = createArray.apply(this, args);
  }

  return arr;
}

function forceGravity(n, sec) {
   if (!n.z) {
    n.y += 9.81 * sec;
    n.x.y += n.y
  }
  return n;
}

function forceScreenBottom(n, bot) {
  if (n.x.y >= bot) {
    n.x.y = bot;
    n.y = 0;
  }
  return n;
}

// TODO: Dealing with overlap
function forceCollisionY(n, bot, end) {
  if (n.x.y >= _.get(state.bottom, Math.floor(n.x.x), bot) && !n.z) {
    n.x.y = _.get(state.bottom, Math.floor(n.x.x), bot);
    n.y = 0;

    // Check here if you can move left or right
    // The randomness of choosing a direction means that sometimes the direction
    // chosen will be a direction the particle can't go
    // This naturally limits how far a particle can travel horizontally (the
    // chance of choosing the "correct" direction several times in a row is
    // fairly low)
    // To prevent the particles from moving too much anyway, we use n.x.z which
    // counts the number of horizontal movements we've done
    // We also don't want to have particles moving around when the pile of
    // particles isn't that tall (in the future this will be difference between
    // tallest and shortest particle stack, or maybe even that difference locally)
    // For local differences, you'd have a memoized function (whose memo gets
    // cleared on every update()
    //
    // In this case, we want the sand to shift one over when it hits the top

    // var rnd = _.random(0, 1);
    // var dir = Math.pow(-1, 1 + rnd);
    // if (n.x.y <= _.get(state.bottom, Math.floor(n.x.x) + dir, bot) && dir * (n.x.x + dir) <= config.canvas.w * rnd && n.x.z <= 5 && n.x.y <= bot - 2) {
    //   n.x.x = n.x.x + dir;
    //   n.x.z += 1;
    // } else {
    //   // n.z freezes any further messing of the particle by the system
    //   n.z = true;
    //   _.set(state.bottom, Math.floor(n.x.x), _.get(state.bottom, Math.floor(n.x.x), bot) - 1);
    // }

    // This is the same code as above, but we check both directions, albeit in
    // a random order

    var rnd = _.random(0, 1);
    var dir = Math.pow(-1, 1 + rnd);

    if (n.x.y <= _.get(state.bottom, Math.floor(n.x.x) + dir, bot) && dir * (n.x.x + dir) <= config.canvas.w * rnd && n.x.z <= 5) {
      n.x.x = Math.floor(n.x.x + dir);
      n.x.z += 1;
    } else if (n.x.y <= _.get(state.bottom, Math.floor(n.x.x) - dir, bot) && -1 * dir * (n.x.x - dir) <= config.canvas.w * (1 - rnd) && n.x.z <= 5) {
      n.x.x = Math.floor(n.x.x - dir);
      n.x.z += 1;
    } else {
      n.z = true;
      _.set(state.bottom, Math.floor(n.x.x), _.get(state.bottom, Math.floor(n.x.x), bot) - 1);
    }
  }
  return n;
}